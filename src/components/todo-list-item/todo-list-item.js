import React, { Component } from 'react';

import './todo-list-item.css';

export default class TodoListItem extends Component {
/***********Old method to save lexical meaning of this word***********/
  // constructor() {
  //   super();
  //   this.onLabelClick = () => {
  //     console.log(`Done: ${ this.props.label }`)
  //   };
  // }
  // constructor() {
  //   super();
  //   this.state = {
  //     done: false
  //   };
  // };
    /***********New method to save lexical meaning of this word***********/
  state = {
    done: false,
    important: false
  };
    onLabelClick = () => {
    this.setState({
      done:true
    });
  };
  onSucessBtnClick = () => {
    this.setState({
      important: true
    });
  }

  render() {
    const { label } = this.props;
    const { done, important } = this.state;
    let classNames = 'todo-list-item';
    if (done) {
      classNames += ' done';
    }
    if (important) {
        classNames += ' important';
    }
  
    return (
      <span className={classNames}>
        <span
          className="todo-list-item-label"
         onClick={ this.onLabelClick }>
          {label} 
        </span>
  
        <button type="button"
                className="btn btn-outline-success btn-sm float-right" onClick={ this.onSucessBtnClick }>
          <i className="fa fa-exclamation" />
        </button>
  
        <button type="button"
                className="btn btn-outline-danger btn-sm float-right">
          <i className="fa fa-trash-o" />
        </button>
      </span>
    );
  };
}
